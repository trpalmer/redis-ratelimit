using System.Threading.Tasks;
using StackExchange.Redis;

namespace Redis
{
    public abstract class RateLimitStrategy : IRateLimit
    {
        private LoadedLuaScript _script;
		private ConnectionMultiplexer _conn;
		protected abstract string RawScript { get; }
		protected int WindowInSeconds { get; private set;}

		public RateLimitStrategy(ConnectionMultiplexer conn, int windowInSeconds)
		{
			_conn = conn;
			WindowInSeconds = windowInSeconds;
		}

		protected IDatabase GetDb() => _conn.GetDatabase();

		protected IServer GetServer()
		{
			return _conn.GetServer(_conn.GetEndPoints()[0]);
		}

		protected LoadedLuaScript Script 
		{
			get 
			{
				if (_script == null)
				{
					var script = LuaScript.Prepare(RawScript);
					_script = script.Load(GetServer());
				}
				return _script;
			}
		}

        public abstract Task<(bool above, int currentValue)> IsOver(string key, int maxRate);
    }
}
