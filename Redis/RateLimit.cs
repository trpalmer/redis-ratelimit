using System.Threading.Tasks;

namespace Redis
{
	public interface IRateLimit
	{
		Task<(bool above, int currentValue)> IsOver(string key, int maxRate);
	}
}
