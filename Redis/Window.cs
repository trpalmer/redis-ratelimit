using System.Threading.Tasks;
using StackExchange.Redis;

namespace Redis
{
    public class Window : RateLimitStrategy, IRateLimit
    {
		protected override string RawScript => @"
        	local current
		    current = redis.call('incr', @key)
		    if tonumber(current) == 1 then
			    redis.call('expire', @key, @windowInSeconds)
		    end
		    return current
        ";
		
		public Window(ConnectionMultiplexer conn, int windowInSeconds) : base(conn, windowInSeconds) {}
		
		private async Task<int> StoreRate(string key)
		{	
			var result = await Script.EvaluateAsync(GetDb(), new { key = (RedisKey)key, windowInSeconds = WindowInSeconds });
            return (int)result;
        }

        public override async Task<(bool above, int currentValue)> IsOver(string key, int maxRate)
        {
            var currentValue = await StoreRate(key);
            return (currentValue > maxRate, currentValue);
        }
    }
}
