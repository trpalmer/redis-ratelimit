﻿using System.Threading.Tasks;
using StackExchange.Redis;

namespace Redis
{
    public static class RateLimitExtensions
    {
        internal const string RateWindowScript = @"
           local current
		    current = redis.call('incr', @key)
		    if tonumber(current) == 1 then
			    redis.call('expire', @key, @windowInSeconds)
		    end
		    return current 
        ";
        private static async Task<int> StoreRateForWindow(this IDatabase db, string key, int windowInSeconds)
        {
            var script = LuaScript.Prepare(RateWindowScript);
            var result = await db.ScriptEvaluateAsync(script, new { key = (RedisKey)key, windowInSeconds });
            return (int)result;
        }

        public static async Task<(bool above, int currentValue)> IsOverWindow(this IDatabase db, string key, int windowInSeconds, int maxRate)
        {
            var currentValue = await db.StoreRateForWindow(key, windowInSeconds);
            return (currentValue > maxRate, currentValue);
        }
    }
}
