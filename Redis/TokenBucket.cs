using System;
using System.Threading.Tasks;
using StackExchange.Redis;

namespace Redis
{
    public class TokenBucket : RateLimitStrategy, IRateLimit
    {
		protected override string RawScript => @"
			local ts = tonumber(@eventTs)
			local min = ts - tonumber(@windowInSeconds)
			local expiresIn = tonumber(@windowInSeconds) + 10
			
			redis.call('ZREMRANGEBYSCORE', @key, '-inf', min)
			redis.call('ZADD', @key, ts, ts)
			redis.call('EXPIRE', @key, expiresIn)

			return redis.call('ZCARD', @key)
        ";
		
		public TokenBucket(ConnectionMultiplexer conn, int windowInSeconds) : base(conn, windowInSeconds) {}
		
		private async Task<int> StoreEvent(string key)
		{	
			var eventTs = ((double)DateTime.Now.Ticks / TimeSpan.TicksPerSecond).ToString();
			var result = await Script.EvaluateAsync(GetDb(), new { key = (RedisKey)key, eventTs, windowInSeconds = WindowInSeconds });
            return (int)result;
        }

        public override async Task<(bool above, int currentValue)> IsOver(string key, int maxRate)
        {
            var currentValue = await StoreEvent(key);
            return (currentValue > maxRate, currentValue);
        }
    }
}
