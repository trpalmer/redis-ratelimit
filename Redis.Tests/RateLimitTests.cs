using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using NUnit.Framework;
using StackExchange.Redis;

namespace Redis.Tests
{
	[TestFixture]
	[Parallelizable]
	public class RateLimitTests
    {
		private static ConnectionMultiplexer _cm;
		private static Dictionary<string, IRateLimit> _strategies;
		private const int WindowInSeconds = 1;

		static RateLimitTests()
		{
			_cm = ConnectionMultiplexer.Connect("localhost,abortConnect=False");
			_strategies = new(StringComparer.InvariantCultureIgnoreCase) 
			{
				["Window"] = new Window(_cm, WindowInSeconds),
				["WindowViaExtention"] = new WindowRateLimitExtensionAdapter(_cm, WindowInSeconds),
				["TokenBucket"] = new TokenBucket(_cm, WindowInSeconds)
			};
		}

		[OneTimeTearDown]
		public static void OneTimeTearDown()
		{
			_cm.Close();
			_cm.Dispose();
		}

        protected string GetTestKey()
        {
            return Guid.NewGuid().ToString("N");
        }

		private static IEnumerable<TestCaseData> StrategyTestCaseData()
		{
			return _strategies.Select(kv => {
				var tc = new TestCaseData(kv.Value, 20);
				tc.SetDescription(kv.Key);
				return tc;
			});
		}

        [TestCaseSource(nameof(StrategyTestCaseData))]
        public async Task TestBelowWindow(IRateLimit strategy, int maxRate)
        {
            string key = GetTestKey();
            for (var i = 0; i < maxRate; i++)
            {
                (bool result, int count) = await strategy.IsOver(key, maxRate);
                Assert.False(result);
            }
        }

		[TestCaseSource(nameof(StrategyTestCaseData))]
        public async Task TestExceedsWindow(IRateLimit strategy, int maxRate)
        {
            string key = GetTestKey();
            bool result;
            for (var i = 0; i < maxRate; i++)
            {
                (result, _) = await strategy.IsOver(key, maxRate);
                Assert.False(result);
            }

            (result, _) = await strategy.IsOver(key, maxRate);
            Assert.True(result);
        }

        [TestCaseSource(nameof(StrategyTestCaseData))]
        public async Task TestWindowResetWait(IRateLimit strategy, int maxRate)
        {
			int closeEnoughFactor = 5;
            string key = GetTestKey();
            bool result;
            var sw = new Stopwatch();
            sw.Start();
            for (var i = 0; i < maxRate; i++)
            {
                (result, _) = await strategy.IsOver(key, maxRate);
                Assert.False(result);
            }

            (result, _) = await strategy.IsOver(key, maxRate);
            Assert.True(result);

            Thread.Sleep((WindowInSeconds * 1000) - (int)sw.ElapsedMilliseconds + closeEnoughFactor);

            (result, _) = await strategy.IsOver(key, maxRate);
            Assert.False(result);
        }

        [TestCaseSource(nameof(StrategyTestCaseData))]
        public async Task TestWindowResetUnderLoad(IRateLimit strategy, int maxRate)
        {
            int closeEnoughFactor = 5;
            string key = GetTestKey();
            bool result;
            var sw = new Stopwatch();
            sw.Start();
            for (var i = 0; i < maxRate; i++)
            {
                (result, _) = await strategy.IsOver(key, maxRate);
                Assert.False(result);
            }

            while (sw.ElapsedMilliseconds <= (WindowInSeconds * 1000) - closeEnoughFactor)
            {
                int count;
                (result, count) = await strategy.IsOver(key, maxRate);
                Assert.True(result, $"windowInSeconds={WindowInSeconds} elapsedMs={sw.ElapsedMilliseconds} count={count}");
                Assert.Greater(count, maxRate);
            }
            Thread.Sleep(1000);

			int oc;
            (result, oc) = await strategy.IsOver(key, maxRate);
            Assert.False(result, $"windowInSeconds={WindowInSeconds} elapsedMs={sw.ElapsedMilliseconds} count={oc}");
        }
    }
}