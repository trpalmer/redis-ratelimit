using System.Threading.Tasks;
using StackExchange.Redis;

namespace Redis.Tests
{
	public class WindowRateLimitExtensionAdapter: IRateLimit
    {
		private IDatabase _db;
		private int _windowInSeconds;

		public WindowRateLimitExtensionAdapter(ConnectionMultiplexer conn, int windowInSeconds)
		{
			_db = conn.GetDatabase();
			_windowInSeconds = windowInSeconds;
		}
        
        public Task<(bool above, int currentValue)> IsOver(string key, int maxRate)
		{
			return _db.IsOverWindow(key, _windowInSeconds, maxRate);
		}
    }
}