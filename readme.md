# Rate Limiting with Redis
This project explores using different methods for rate limiting using Redis and includes some background on choices and why they were made. It is implemented in C#/.NET core using the StackExchange.Redis library, async calls, and includes unit tests to confirm behavior using a local Redis server.

## Basic Rate Limiting Information
Rate limiting can be used to delay or reject calls to protect a resource from overload. As the name implies, the caller is limited to a certain rate (calls per unit of time). There are several implementation methods, each with certain strengths and weaknesses. 

All these methods use a rolling time window to achieve their goal, however the size of the window chosen can also impact the system you are trying to protect. If the window chosen is too large, a large number of calls can be made in a short period potentially overwhelming the system you are trying to protect while still being within the defined limit. And example might be 480 calls per minute (CPM) where 470 calls are made within the first second. This rate could also be expressed as 8 calls per second (CPS) which would be the equivalent number of calls per minute, but limits the system to prevent spikes for a constant rate. Which rate you choose will have more to to with the system you are protecting, the expectations you have set for your clients, and how you express the rate vs how it is enforced.

## Using Redis
Redis as an extremely fast distributed cache supports data structures to efficiently implement rate limiting with one of it's main drawbacks that commands are not atomic. Redis does support Lua script, which are executed atomically and solves this problem. All the implementations in this project use Lua scripts in Redis. Redis provides a [basic rate limiting pattern example](https://redislabs.com/redis-best-practices/basic-rate-limiting/), which does not use a rolling window and requires more keys and client side key manipulation.

## Simple Window
This solution uses a single key for a bucket. The key expires based on the designated window and it uses an increment counter to return the number of calls during that window. The script is called with two parameters: the key tied to what/who you are rate limiting, and the size of the window in seconds. While simple, this solution lacks precision due to it's requirement on [Redis EXPIRE algorithm](https://redis.io/commands/expire#appendix-redis-expires) which since Redis 2.6 is 0 to 1 ms.

```lua
local current
current = redis.call('incr', KEYS[1])
if tonumber(current) == 1 then
    redis.call('expire', KEYS[1], ARGV[1])
end
return current
```

The first call sets the expiration based on the window size. If [`INCR`](https://redis.io/commands/incr) is called on a non-existent key it will return 1. This resets the key for the next window. 

## Token Bucket
This solution uses a key for a bucket, tracking a sorted set of events by timestamp which must have below second precision. This could be a unix timestamp such as `date +%s.%N` or on Ticks/Ticks per second on windows `[DateTime]::Now.Ticks / [double][TimeSpan]::TicksPerSecond`. We remove all items of the set outside the window, add the new event, and return the number of events. For a 1 second windows, this gives us the highest precision rolling window. The script is called with three parameters: the key tied to what/who you are rate limiting, the timestamp of your event as a string, and your window.

```lua
local ts = tonumber(ARGV[1]) -- timestamp
local min = ts - tonumber(ARGV[2]) -- window size in seconds
local expiresIn = tonumber(ARGV[2]) + 10

redis.call('ZREMRANGEBYSCORE', KEYS[1], '-inf', min)
redis.call('ZADD', KEYS[1], ts, ts)
redis.call('EXPIRE', KEYS[1], expiresIn)

return redis.call('ZCARD', KEYS[1])
```

* The event timestamp is first converted to a number
* The minimum timestamp allowed in the window is calculated by subtracting the window size
* [`ZREMRANGEBYSCORE`](https://redis.io/commands/zremrangebyscore) is called removing all elements in the set between `-inf` (-infinity) and `min`, removing all items below min
* [`ZADD`](https://redis.io/commands/zadd) is called adding the current event
* [`EXPIRE`](https://redis.io/commands/expire) is called to expire the key as house keeping, technically not needed
* [`ZCARD`](https://redis.io/commands/zcard) is called to get the number of elements in the set and returned

## Implementation Details
* Implemented as a strategy pattern implementing `IRateLimit` which allows the algorithm used and tested to be switched.
* `RateLimitStrategy` base class implements some details of Redis/StackExchange.Redis taking advantage of:
    * Prepared scripts, for easier syntax and a hash using [`SCRIPT LOAD`](https://redis.io/commands/script-load)
    * EvaluateScript which uses  [`EVALSHA`](https://redis.io/commands/evalsha) which prevents the script from being sent to the server each time.
* The Window algorithm is also implemented as extension methods with an adapter.
* Tests are run in parallel

## Sources
* [CALLR: Rate Limiting for distributed systems with Redis and Lua](https://blog.callr.tech/rate-limiting-for-distributed-systems-with-redis-and-lua/)
